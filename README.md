# Samostatné cvičení: LU dekompozice

Projekt lze stáhnou buď přímo pomocí gitu, nebo jako [zip soubor](https://gitlab.com/python-fjfi/projekt-lu-dekompozice/-/archive/main/projekt-lu-dekompozice-main.zip).

Vaším úkolem je:
1. Nainstalovat potřebné závislosti příkazem
```
python -m pip install -r requirements.txt
```
případně, pokud používáte condu, příkazem
```
conda install --file requirements.txt
```
2. Ověřit, že úspěšně spustíte testy příkazem
```
python3 -m pytest .
```
3. Dopsat jeden nebo více testů, které odhalí chybu v kódu.
4. Opravit nalezenou chybu v kódu.

Rozšířené úkoly ("nepovinné"):
5. Pokuste se kód zefektivnit (zrychlit), zejména pro velké matice.
6. Zamyslete se, jak jsou (nejsou) řešené validace, např. co se stane v případě singulární matice.
Validace se pokuste vylepšit.
