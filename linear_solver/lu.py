import numpy as np

def lu_decomposition(A):
    """
    LU decomposition of a matrix A using Doolittle's method.
    """
    n = len(A)
    L = np.zeros((n, n))
    U = np.zeros((n, n))
    for i in range(n):
        L[i, i] = 1  # diagonal elements of L are 1
        for j in range(i, n):
            U[i, j] = A[i, j] - sum(L[i, k] * U[k, j] for k in range(i))
        for j in range(i + 1, n):
            L[j, i] = (A[j, i] - sum(L[j, k] * U[k, i] for k in range(i))) * U[i, i]
    return L, U

def lu_solver(A, b):
    """
    Solves the linear system Ax=b using LU decomposition.
    """
    L, U = lu_decomposition(A)
    y = np.zeros(len(b))
    x = np.zeros(len(b))
    # Forward substitution for Ly = b
    for i in range(len(b)):
        y[i] = b[i] - sum(L[i, j] * y[j] for j in range(i))
    # Backward substitution for Ux = y
    for i in range(len(b) - 1, -1, -1):
        x[i] = (y[i] - sum(U[i, j] * x[j] for j in range(i + 1, len(b)))) / U[i, i]
    return x
