import numpy as np
from numpy.testing import assert_allclose
from linear_solver.lu import lu_solver


def test_lu_solver():
    # A simple matrix example
    A = np.array([[1, 0, 0], [0, 2, 0], [0, 0, 3]])
    b = np.ones(3)
    x = lu_solver(A, b)
    # check that A*x = b
    assert_allclose(np.dot(A, x), b)
    # check that x is the known solution
    assert_allclose(x, np.array([1, 1/2, 1/3]))
